<div class="footer" style="background:DarkSlateGrey">
	<div class="container">
		<div id="credits" class="copyright pull-left" style="padding-top:20px;color:#F0F0F0">  &copy; 2015. Developed by Alex Thorburn-Winsor and Harry Dong </div> 
	<div class="pull-right">
		<a style="text-decoration:none;padding:10px" href="https://twitter.com/intent/tweet?text=Find%20out%20how%20long%20it%20takes%20you%20to%20read%20any%20book&url=http://howlongtoreadthis.com/" class="icon-button twitter" target="_blank"><i class="icon-twitter"></i><span></span></a>
		<a style="text-decoration:none;margin-top:10px" href="https://www.facebook.com/sharer/sharer.php?u=http://www.howlongtoreadthis.com" class="icon-button facebook" target="_blank"><i class="icon-facebook"></i><span></span></a>
        </div>
</div>
