<!DOCTYPE html>
<html dir="ltr" lang="en" class="off-canvas">
<head>
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta charset="UTF-8">
<title>How it Works</title>

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link href='//fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
<link href="//fonts.googleapis.com/css?family=RobotoDraft:100,300,400,500,700" rel=
    "stylesheet">
<link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css'>
<link rel="stylesheet" type="text/css" href="./css/newstyles.css">
</head>

<body>
<div class="wrapper" style="margin-bottom:2px">
	<?php include_once('header.php'); ?>
	<div class="container">
		<h1 style="color:white">How it Works</h1>
		<br>
		<div class="well well-lg">
			<p style="text-align:justify;font-size:14px; color:#34495e; line-height:2">
			<b>1. </b>How Long to Read is a search engine that allows you to find almost any book and get your reading time for each one. 
			Use the search bar on the home page to search for any of over 12 million books on How Long to Read.
			<br>
			<br>
			<img src="HLTR_search.gif" alt="How to use How Long to Read">
			<br>
			<b><br>2. </b>On the search results page you'll find a general overview of all of the books matching your search term. Click on the book's cover image to go to its page and find your reading time for each one. 
			<br><br>
			<img src="howtouse1.png" alt="How to use How Long to Read">
			<br><br>
			<b>3. </b>Once you've found the book you're looking for on its page you will find the average readers reading time for the book, and on the right you will find a button to take a reading speed test and find your unique reading time for the book.
			<br><br>
			<img src="HLTR_test.gif" alt="How to use How Long to Read">
			<br><br>
			<b>4. </b>Once you're done reading click stop and in the popup your specific reading time will be displayed, along with your average reading speed. 
			Once you've taken the reading speed test your reading time for the book will be saved in your browser, so if you ever want to check your time again your reading time will be saved and ready to view.
			<br><br>
			<img src="howtouse3.png" alt="How to use How Long to Read">
			</p>
		</div>
		<!DOCTYPE html>
<html dir="ltr" lang="en" class="off-canvas">
<head>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "042fe7c8-61a0-432b-9f9a-3808a2ef5415", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="description" content="Search over 12 million books and find your reading time for each one with How Long to Read.">
<meta charset="UTF-8">
<title>How Long to Read</title>

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link href='//fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
<link href="//fonts.googleapis.com/css?family=RobotoDraft:100,300,400,500,700" rel=
    "stylesheet">
<link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css'>
<link rel="stylesheet" type="text/css" href="./css/newstyles.css">

</head>


<body>
<div class="wrapper">
	<?php include_once('header.php'); ?>
	<?php include_once("analyticstracking.php") ?>
	<div class="container">
		<div class="row-fluid" style="margin-bottom:20px; text-align:center;">
			<div class="span12">
				<img style="width:500px;height:auto;max-width:100%" src="http://www.howlongtoreadthis.com/images/logo.png">
				<div class="spacer10"></div>
				<h2 style="color:white;">How Long to Read</h2>
				<div class="spacer10"></div>
				<form action="search_result.php" method="get">
					<div class="form-group">
						<input type="text" class="form-control" name="search_keyword" id="search_keyword" style="height:45px;-moz-border-radius: 5px;-webkit-border-radius: 5px;border-radius: 5px" autofocus="autofocus" maxlength="120" placeholder="Search over 12 million books">
					</div>
					<center><input type="submit" class="btn btn-default btn-large" value="Search"></center>
				</form>
				<div style="margin-top:60px;"> 
					<p style="margin-top:20px;text-align:center">Search over 12 million books and find your reading time for each one.</p></div>
				</div>
			</div>
		</div>
		<div class="push"></div>
	</div>
</div>
<?php include_once('footer.php'); ?>
</body>
</html>

	</div>
</div>
<?php include_once('footer.php'); ?>
</body>
</html>
